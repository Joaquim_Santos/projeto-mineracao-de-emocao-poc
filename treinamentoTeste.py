import pandas as pd
import numpy
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn import metrics
from sklearn.model_selection import cross_val_predict
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import label_binarize
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
import seaborn as sn
import sys

def ler_entrada_treino():
    try:
        dataset = pd.read_csv('opinioes_usuarios.csv')
        opinioes = dataset['Opinião'].values
        classes = dataset['Sentimento'].values
    except Exception as excecao:
        print("Ocorreu uma exceção ao ler o arquivo")
        print(f"Tipo da exceção: {type(excecao)}")
        print("Descrição da exceção:\n\n")
        print(excecao)
        sys.exit()

    '''
    tweets = []
    classes = []
    with open("opinioes_usuarios.txt", "rt") as f:
        for frase in f.read().split('\n'):
            frase = frase.split('---')
            tweets.append(frase[1])
            classes.append(frase[2])
    '''

    return opinioes, classes

def construir_modelo_sentimento(listaFrases, classes):
    listaFrases = numpy.array(listaFrases)
    classes = numpy.array(classes)
    vectorizer = CountVectorizer(ngram_range=(1, 1))
    freq_tweets = vectorizer.fit_transform(listaFrases)
    modelo = MultinomialNB(alpha=3.0)
    modelo.fit(freq_tweets, classes)

    return modelo, vectorizer, freq_tweets

def avaliar_desempenho(modelo, frequencia_frases, classes):
    resultados = cross_val_predict(modelo, frequencia_frases, classes, cv=10)
    acuracia = metrics.accuracy_score(classes, resultados)
    print(f"A acurácia do mmodelo treinado com a base em questão é {acuracia*100}%\n")

    sentimentos = ["Positivo", "Negativo", "Neutro"]
    metricas = metrics.classification_report(classes, resultados, sentimentos)
    print("As métricas obtidas para o modelo são")
    print(metricas)
    print('\n')
    
    sentimentos = ["Negativo", "Neutro", "Positivo"]
    matrizConfusao2 = pd.crosstab(classes, resultados, rownames=["Real"], colnames=["Predito"], margins=True)
    matrizConfusao = confusion_matrix(classes, resultados)
    matrizConfusao = pd.DataFrame(matrizConfusao)
    matrizConfusao.index = sentimentos
    matrizConfusao.columns = sentimentos
    print("Matrz de Confusão para o modelo\n")
    print(matrizConfusao2)
    #plt.figure(figsize = (10,10))
    #fig, ax = plt.subplots(figsize=(50,50))
    sn.set(font_scale=1.6)
    figura_matriz = sn.heatmap(matrizConfusao, annot=True,linewidths=2, vmax=3000, vmin=0, square=True)# font size
    plt.show()
    #figure = figura_matriz.get_figure()    
    #figure.savefig('matriz_sentimentos.png', dpi=400)

    #Claculando AUC e construindo Curva ROC
    Classes = classes
    Classes = label_binarize(Classes, classes=sentimentos)
    n_classes = 3

    classes_score = modelo.predict_proba(frequencia_frases)
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(n_classes):
        fpr[i], tpr[i], _ = roc_curve(Classes[:, i], classes_score[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    
    sentimentos = ['Neutro', 'Negativo', 'Positivo']
    for i in range(n_classes):
        plt.figure()
        plt.plot(fpr[i], tpr[i], label='Curva ROC (área = %0.2f)' % roc_auc[i])
        plt.plot([0, 1], [0, 1], 'k--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('Taxa de Falso Positivo')
        plt.ylabel('Taxa de Verdadeiro Positivo')
        plt.title('Curva ROC para Predições da Polaridade ' + sentimentos[i])
        plt.legend(loc="lower right")
        plt.show()
