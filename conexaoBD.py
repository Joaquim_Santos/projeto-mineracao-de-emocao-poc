import pymysql.cursors

class operacoesBD(object):
    def selecionar_todas__opnioes(self):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='scape',cursorclass=pymysql.cursors.DictCursor)

        try:
            with conexao.cursor() as cursor:
                comandoSQL = "SELECT ID, opiniao, probabilidade_positivo FROM opiniao;"
                cursor.execute(comandoSQL)
                opnioesRetornadas = cursor.fetchall()
                return opnioesRetornadas
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a consulta ao Banco MySQL")
            print(f"Tipo da exceção: {type(excecao)}")
            print("Descrição da exceção:\n")
            print(excecao)
            return None

    def inserir_probabilidade(self, ID_OPNIAO, prob_positivo, prob_neutro, prob_negativo, sentimento):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='scape',cursorclass=pymysql.cursors.DictCursor)

        try:
            with conexao.cursor() as cursor:
                comandoSQL = "UPDATE opiniao SET probabilidade_positivo = %s, probabilidade_neutro = %s, probabilidade_negativo = %s, sentimento = %s WHERE ID = %s;"
                cursor.execute(comandoSQL, (prob_positivo, prob_neutro, prob_negativo, sentimento, ID_OPNIAO))
                conexao.commit()
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a inserção de probabilidade no Banco MySQL")
            print(f"Tipo da exceção: {type(excecao)}")
            print("Descrição da exceção:\n")
            print(excecao)

    def selecionar_probs_evento(self):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='scape', cursorclass=pymysql.cursors.DictCursor)

        try:
            with conexao.cursor() as cursor:
                comandoSQL = "SELECT ID_EVENTO, probabilidade_positivo, probabilidade_neutro, probabilidade_negativo FROM opiniao WHERE probabilidade_positivo IS NOT NULL ORDER BY ID_EVENTO;"
                cursor.execute(comandoSQL)
                probsRetornadas = cursor.fetchall()
                return probsRetornadas
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a consulta de probabilidades de evento ao Banco MySQL")
            print(f"Tipo da exceção: {type(excecao)}")
            print("Descrição da exceção:\n")
            print(excecao)
            return None

    def inserir_score(self, ID, score_evento):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='scape',cursorclass=pymysql.cursors.DictCursor)

        try:
            with conexao.cursor() as cursor:
                comandoSQL = "UPDATE evento SET score_evento = %s WHERE ID = %s;"
                cursor.execute(comandoSQL, (score_evento, ID))
                conexao.commit()
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a inserção de score no Banco MySQL")
            print(f"Tipo da exceção: {type(excecao)}")
            print("Descrição da exceção:\n")
            print(excecao)

    def selecionar_score_evento(self, ID_evento):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='scape',
                                  cursorclass=pymysql.cursors.DictCursor)

        try:
            with conexao.cursor() as cursor:
                comandoSQL = "SELECT score_evento FROM Evento WHERE ID = %s;"
                cursor.execute(comandoSQL, (ID_evento))
                evento = cursor.fetchall()
                comandoSQL = 'SELECT COUNT(*) FROM Evento JOIN opiniao WHERE Evento.ID = ID_EVENTO AND Evento.ID = %s AND sentimento LIKE \'Positivo\';'
                cursor.execute(comandoSQL, (ID_evento))
                quant_positivo = cursor.fetchall()
                comandoSQL = 'SELECT COUNT(*) FROM Evento JOIN opiniao WHERE Evento.ID = ID_EVENTO AND Evento.ID = %s AND sentimento LIKE \'Negativo\';'
                cursor.execute(comandoSQL, (ID_evento))
                quant_negativo = cursor.fetchall()
                comandoSQL = 'SELECT COUNT(*) FROM Evento JOIN opiniao WHERE Evento.ID = ID_EVENTO AND Evento.ID = %s AND sentimento LIKE \'Neutro\';'
                cursor.execute(comandoSQL, (ID_evento))
                quant_neutro = cursor.fetchall()
                return evento[0], quant_positivo[0], quant_negativo[0], quant_neutro[0]
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a consulta de score de evento ao Banco MySQL")
            print(f"Tipo da exceção: {type(excecao)}")
            print("Descrição da exceção:\n")
            print(excecao)
            return None, None, None, None

    def selecionar_eventos_prob_nula(self):
        conexao = pymysql.connect(host='127.0.0.1', user='root', passwd='', db='scape',
                                  cursorclass=pymysql.cursors.DictCursor)

        try:
            with conexao.cursor() as cursor:
                comandoSQL = "SELECT ID_EVENTO, probabilidade_positivo, probabilidade_neutro, probabilidade_negativo FROM opiniao JOIN Evento WHERE Evento.ID = ID_EVENTO and probabilidade_positivo IS NOT NULL AND score_evento IS NULL ORDER BY ID_EVENTO;"
                cursor.execute(comandoSQL)
                probsRetornadas = cursor.fetchall()
                return probsRetornadas
        except Exception as excecao:
            print("ocorreu uma exceção ao realizar a consulta de probabilidades de evento ao Banco MySQL")
            print(f"Tipo da exceção: {type(excecao)}")
            print("Descrição da exceção:\n")
            print(excecao)
            return None