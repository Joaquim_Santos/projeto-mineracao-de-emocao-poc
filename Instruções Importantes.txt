1) Arquivo Principal
--> O arquivo analisaSentimento.py � o "main" da aplica��o. Executando este ser� feito todo o processo para proceder com a an�lise de sentimentos.
Este ir� fazer a leitura dos dados rotulados de entrada "opinioes_usuarios.csv", invocar os m�todos para limpeza e remo��o de ru�dos e ent�o
construir o modelo treinado baseado em Naive Bayes da biblioteca Sklearn. Este ser� ent�o aplicado sobre as opini�es de evento da base de dados para realizar a predi��o
e c�lculo de scores.

2) Cria��o do Modelo
--> O Modelo de treinamento � constru�do no c�digo do arquivo constroiModelo.py. Este gera o modelo em si (modeloSentimento), um vetor que trata das 
probabilidades de ocorr�ncia das palavras (vetorProbabilidade) e uma lista contendo as ocorr�ncias de palavras para cada frase lida do arquivo de entrada rotulado (frequencia_frases).
Estes tr�s objetos s�o necess�rios para cria��o e uso do modelo de an�lise. Feito isto, os tr�s s�o gravados em disco, no diret�rio da aplica��o, gerando os arquivos:
-modelo_sentimento.joblib
-vetor_probabilidade.joblib
-frequencia_frases.joblib

--> Isto � feito utilizando o m�todo dump da biblioteca joblib, para realizar a persist�ncia dos modelos. Estes tr�s arquivos gerados s�o representa��es dos modelos criados, em disco. N�O DEVEM SER MODIFICADOS MANUALMENTE!
O m�todo deste arquivo respons�vel por isto � invocado pelo c�digo do arquivo analisaSentimento.py. Logo no in�cio deste � feito um try-except. Caso os modelos j� existam apenas � feito o carregamento destes no try.
Caso  contr�rio, o c�digo dentro do except invoca o m�todo para constru��o dos modelos e os carrega em seguida. 
� necess�rio criar o modelo apenas uma vez. Desta forma, para us�-lo em qualquer outra execu��o, � usado o m�todo load da joblib (dentro do try), para carregar os modelos anteriormente gravados.
Feito isto, eles j� podem ser usados diretamente.

--> Caso seja criado um novo modelo, com base em um novo arquivo de entrada ou em novas configura��es, ent�o deve-se apagar os modelos gravados no diret�rio,
assim, ao executar o arquivo python principal, os novos modelos definidos ser�o gravados.

-->Para alterar o modelo existente, a fim de se criar um novo, devem ser feitas altera��es no arquivo treinamentoTeste.py, o qual define os m�todos que configuram as caracter�sticas do modelo. 

3)Conjunto de treinamento
--> O arquivo "opinioes_usuarios.csv", no diret�rio do projeto, � o que ser� utilziado para constru��o do modelo de treino. Se trata de um arquivo csv contendo uma cole��o de opini�es extra�das da base de dados do MyMobiConf ,
classiifcadas como Positivo, Negativo ou Neutro. Este arquivo  � composto de quatro colunas, ID (que se refere ao identificador da opini�o na base de dados), Opini�o(que se trata da pr�pria opini�o do usu�rio), 
Sentimento (que pode possuir os valores "Positivo", "Negativo" ou "Neutro", sendo a classifica��o de sentimento de cada frase feita manualmente) e uma coluna que � apenas uma numera��o gerada pela biblioteca pandas para arquivo csv.

-->As colunas que realmente importam s�o Opini�o e Sentimento, pois s�o as usadas para constru��o do modelo de treino. Deste modo, qualquer arquivo csv pode ser usado, contendo qualquer n�mero de colunas, pois apenas ser�o consideradas estas duas.
Para tanto, o arquivo a ser usado como entrada deve seguir o formato csv e ter essas duas colunas exatamente com os nomes Opini�o e Sentimento, e o arquivo deve ter o nome opinioes_usuarios.csv.
Al�m disto, os valores da coluna Opini�o devem ser sempre textos entre aspas duplas, para que n�o haja problemas na leitura da biblioteca pandas, no caso do texto conter v�rgulas ou caracteres especiais.
J� os valores da coluna Sentimento devem ser um dos tr�s: "Positivo", "Negativo", ou "Neutro". Devem estar escritos exatamente assim, sem aspas e respeitando mai�sculas e min�sculas.
Tamb�m pode se modificar os dados do arquivo j� existente, contanto que se respeite aos padr�es mencionados acima.



