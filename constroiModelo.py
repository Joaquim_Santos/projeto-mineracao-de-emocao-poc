import limpezaDeDados
import treinamentoTeste
#from sklearn.externals.joblib import dump Depreciada
from joblib import dump
import numpy

def gerar_gravar_modelo():
    frases, classes = treinamentoTeste.ler_entrada_treino()
    frases_sem_ruindo = []
    classes_sem_ruido = []
    for x in range(0, len(frases)):
        if (limpezaDeDados.verificar_ruido(frases[x])):
            frases_sem_ruindo.append(frases[x])
            classes_sem_ruido.append(classes[x])

    frases_limpas = limpezaDeDados.limpar_texto(frases_sem_ruindo)
    classes_limpas = classes_sem_ruido
    #Queda de 1% de precisão ao aplicar o stemmer
    #tweetsLimpos = limpezaDeDados.aplicarStemmer(tweetsLimpos)
    modeloSentimento, vetorProbabilidade, frequencia_frases = treinamentoTeste.construir_modelo_sentimento(frases_limpas, classes_limpas)

    classes_limpas = numpy.array(classes_limpas)
    treinamentoTeste.avaliar_desempenho(modeloSentimento, frequencia_frases, classes_limpas)

    #Salvando Modelos de treinamento criados em disco
    dump(modeloSentimento, 'modelo_sentimento.joblib')
    dump(vetorProbabilidade, 'vetor_probabilidade.joblib')
    dump(frequencia_frases, 'frequencia_frases.joblib')
